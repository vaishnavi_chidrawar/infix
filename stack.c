#include "stack.h"
void push(stack *s, int num) {
	s->a[s->k]= num;
	(s->k)++;
}
int pop(stack *s) {
	int t = s->a[s->k - 1];
	(s->k)--;
	return t;
}
int empty(stack *s) {
	return s->k == 0;
}
int full(stack *s) {
	return s->k == MAX;
}
void init(stack *s) {
	s->k = 0;
}
void push1(stack1 *s, char c) {
	s->b[s->j]= c;
	(s->j)++;
}
int pop1(stack1 *s) {
	char t = s->b[s->j - 1];
	(s->j)--;
	return t;
}
int empty1(stack1 *s) {
	return s->j == 0;
}
int full1(stack1 *s) {
	return s->j == MAX;
}
void init1(stack1 *s) {
	s->j = 0;
}

